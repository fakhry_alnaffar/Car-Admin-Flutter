import 'package:car_admin_project/responsive/size_config.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

Widget homeWidget(
    {required var icon,
    required String tittle,
    required Color iconColor}) {
  return Stack(
    children: [
      Container(
        width: SizeConfig().scaleWidth(171),
        height: SizeConfig().scaleHeight(171),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(14),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.16),
              spreadRadius: 0,
              blurRadius: 4,
              offset: Offset(0, 4), // changes position of shadow
            ),
          ],
        ),
      ),
      Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              height: SizeConfig().scaleHeight(59),
              width: SizeConfig().scaleWidth(59),
              child: Icon(
                icon,
                color: iconColor,
                size: SizeConfig().scaleWidth(40),
              ),
              decoration: BoxDecoration(
                color: Color(0xFFF0F4FD),
                borderRadius: BorderRadius.circular(14),
              ),
            ),
            SizedBox(
              height: SizeConfig().scaleHeight(16),
            ),
            Text(
              tittle,
              style: TextStyle(
                fontSize: SizeConfig().scaleTextFont(16),
                fontWeight: FontWeight.w500,
                color: Color(0xff0B204C),
              ),
            ),
            SizedBox(
              height: SizeConfig().scaleHeight(8),
            ),
          ],
        ),
      )
    ],
  );
}

Widget showDistributors({
  // required List<DocumentSnapshot> documents,
  required int index,
  required BuildContext context,
  required String title,
  required String subtitle,
  required String adders,
  String? url,
  required Widget widget1,
  required Widget widget2,
}) {
  return Container(
    padding: EdgeInsetsDirectional.zero,
      margin: EdgeInsetsDirectional.zero,
      clipBehavior: Clip.antiAlias,
      // alignment: AlignmentDirectional.topCenter,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 10.0, bottom: 20),
        child: Container(
          height: SizeConfig().scaleHeight(90),
          width: SizeConfig().scaleWidth(366),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(14),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.baseline,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            textBaseline: TextBaseline.ideographic,
            textDirection: TextDirection.rtl,
            children: [
              SizedBox(
                width: 15,
              ),
              widget1,
              SizedBox(
                width: 10,
              ),
              widget2,
              Spacer(),
              SizedBox(
                width: 0,
              ),
              SizedBox(
                width: 150,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  textBaseline: TextBaseline.alphabetic,
                  textDirection: TextDirection.rtl,
                  children: [
                    Text(
                      title,
                      locale: Locale('ar'),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          textBaseline: TextBaseline.ideographic),
                      textDirection: TextDirection.rtl,
                      textAlign: TextAlign.start,
                    ),
                    Text(
                      subtitle,
                      locale: Locale('ar'),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      textDirection: TextDirection.ltr,
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          color: Color(0xff0B204C),
                          fontSize: 14,
                          textBaseline: TextBaseline.ideographic),
                    ),
                    Text(
                      adders,
                      locale: Locale('ar'),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: Colors.grey.shade600,
                          fontSize: 13,
                          textBaseline: TextBaseline.ideographic),
                      textDirection: TextDirection.rtl,
                      textAlign: TextAlign.start,
                    ),
                  ],
                ),
              ),
              SizedBox(width: 10,),
              Padding(
                padding: const EdgeInsets.only(top: 0, bottom: 0, left: 16),
                child: Container(
                  clipBehavior: Clip.antiAlias,
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100),
                  ),
                  child: url == null ? Icon(Icons.person) : Image.network(url, fit: BoxFit.cover,),
                ),
              ),
            ],
          ),
        ),
      ));
}
Widget selectWidget({
  required String tittle,
  required Widget widget,
}) {
  return Container(
    decoration: BoxDecoration(
      color: Color(0xff1DB854),
      borderRadius: new BorderRadius.only(
        topLeft: const Radius.circular(10.0),
        topRight: const Radius.circular(10.0),
        bottomRight: const Radius.circular(10.0),
        bottomLeft: const Radius.circular(10.0),
      ),
    ),
    height: 140,
    width: double.infinity,
    child: Align(
      alignment: AlignmentDirectional.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          widget,
          Text(
            tittle,
            style: TextStyle(
                color: Colors.white, fontSize: 24, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    ),
  );
}

Widget showAdmins({
  // required List<DocumentSnapshot> documents,
  required int index,
  required BuildContext context,
  required String title,
  required String subtitle,
  required String adders,
  String? url,
}) {
  return Container(
      padding: EdgeInsetsDirectional.zero,
      margin: EdgeInsetsDirectional.zero,
      clipBehavior: Clip.antiAlias,
      // alignment: AlignmentDirectional.topCenter,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 0.0, bottom: 0),
        child: Container(
          height: SizeConfig().scaleHeight(90),
          width: SizeConfig().scaleWidth(366),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(14),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.baseline,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            textBaseline: TextBaseline.ideographic,
            textDirection: TextDirection.rtl,
            children: [
              SizedBox(
                width: 15,
              ),
              SizedBox(
                width: 10,
              ),
              Spacer(),
              SizedBox(
                width: 0,
              ),
              SizedBox(
                width: 150,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  textBaseline: TextBaseline.alphabetic,
                  textDirection: TextDirection.rtl,
                  children: [
                    Text(
                      title,
                      locale: Locale('ar'),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          textBaseline: TextBaseline.ideographic),
                      textDirection: TextDirection.rtl,
                      textAlign: TextAlign.start,
                    ),
                    Text(
                      subtitle,
                      locale: Locale('ar'),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      textDirection: TextDirection.ltr,
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          color: Color(0xff0B204C),
                          fontSize: 14,
                          textBaseline: TextBaseline.ideographic),
                    ),
                    SizedBox(
                      height: 1,
                    ),
                    Text(
                      adders,
                      locale: Locale('ar'),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: Colors.grey.shade600,
                          fontSize: 13,
                          textBaseline: TextBaseline.ideographic),
                      textDirection: TextDirection.rtl,
                      textAlign: TextAlign.start,
                    ),

                    SizedBox(
                      height: 1,
                    ),
                  ],
                ),
              ),
              SizedBox(width: 10,),
              Padding(
                padding: const EdgeInsets.only(top: 5, bottom: 0, left: 16),
                child: Container(
                  clipBehavior: Clip.antiAlias,
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100),
                  ),
                  child: url == null ? Icon(Icons.person) : Image.network(url, fit: BoxFit.cover,),
                ),
              ),
            ],
          ),
        ),
      ));
}
Widget showItem2({
  required BuildContext context,
  required String title,
  required String subtitle,
  String? url,
  required int index,
  // Locale  = Locale('ar'),
  required List<DocumentSnapshot> documents,
}) {
  return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 3.0, bottom: 3),
        child: Container(
          height: SizeConfig().scaleHeight(90),
          width: SizeConfig().scaleWidth(366),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(14),
          ),
          child: Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  textBaseline: TextBaseline.ideographic,
                  children: [
                    Text(
                      title,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                      style: TextStyle(
                          color: Color(0xff0B204C),
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                          textBaseline: TextBaseline.ideographic),
                      textDirection: TextDirection.rtl,
                      textAlign: TextAlign.start,
                      locale: Locale('ar'),
                    ),
                    SizedBox(
                      height: 3,
                    ),
                    Text(
                      subtitle,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: Color(0xff919BB3),
                          fontSize: 13,
                          textBaseline: TextBaseline.ideographic),
                    ),
                  ],
                ),
              ),
              SizedBox(width: 5,),
              Padding(
                padding: const EdgeInsets.only(top: 16, bottom: 16, left: 0),
                child: CircleAvatar(
                  backgroundImage: url != null ? NetworkImage(url) : null,
                  radius: 25,
                  child: url == null ? Icon(Icons.person) : null,
                  backgroundColor: Color(0xff5A55CA),
                ),
              ),
            ],
          ),
        ),
      ));
}

Widget addAdmins({
  // required List<DocumentSnapshot> documents,
  required int index,
  required BuildContext context,
  required String title,
  required String subtitle,
  required String adders,
  String? url,
}) {
  return Container(
      padding: EdgeInsetsDirectional.zero,
      margin: EdgeInsetsDirectional.zero,
      clipBehavior: Clip.antiAlias,
      // alignment: AlignmentDirectional.topCenter,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 0.0, bottom: 0),
        child: Container(
          height: SizeConfig().scaleHeight(90),
          width: SizeConfig().scaleWidth(366),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(14),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.baseline,
            mainAxisAlignment: MainAxisAlignment.center,
            textBaseline: TextBaseline.ideographic,
            textDirection: TextDirection.rtl,
            children: [
              Spacer(),
              SizedBox(
                width: 150,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  mainAxisAlignment: MainAxisAlignment.center,
                  textBaseline: TextBaseline.alphabetic,
                  textDirection: TextDirection.rtl,
                  children: [
                    Text(
                      title,
                      locale: Locale('ar'),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          textBaseline: TextBaseline.ideographic),
                      textDirection: TextDirection.rtl,
                      textAlign: TextAlign.start,
                    ),
                    Text(
                      subtitle,
                      locale: Locale('ar'),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      textDirection: TextDirection.ltr,
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          color: Color(0xff0B204C),
                          fontSize: 14,
                          textBaseline: TextBaseline.ideographic),
                    ),
                    SizedBox(
                      height: 1,
                    ),
                    Text(
                      adders,
                      locale: Locale('ar'),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: Colors.grey.shade600,
                          fontSize: 13,
                          textBaseline: TextBaseline.ideographic),
                      textDirection: TextDirection.rtl,
                      textAlign: TextAlign.start,
                    ),


                  ],
                ),
              ),
              SizedBox(width: 10,),
              Padding(
                padding: const EdgeInsets.only(top: 10, bottom: 0, left: 16),
                child: Container(
                  clipBehavior: Clip.antiAlias,
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100),
                  ),
                  child: url == null ? Icon(Icons.person) : Image.network(url, fit: BoxFit.cover,),
                ),
              ),
            ],
          ),
        ),
      ));
}
Widget showMerchant({
  // required List<DocumentSnapshot> documents,
  required int index,
  required BuildContext context,
  required String title,
  required String subtitle,
  required String adders,
  required Widget widget,
  String? url,
}) {
  return Container(
      padding: EdgeInsetsDirectional.zero,
      margin: EdgeInsetsDirectional.zero,
      clipBehavior: Clip.antiAlias,
      // alignment: AlignmentDirectional.topCenter,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 0.0, bottom: 0),
        child: Container(
          height: SizeConfig().scaleHeight(90),
          width: SizeConfig().scaleWidth(366),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(14),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.baseline,
            mainAxisAlignment: MainAxisAlignment.center,
            textBaseline: TextBaseline.ideographic,
            textDirection: TextDirection.rtl,
            children: [
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 0, bottom: 0, left: 16),
                    child: widget,
                  ),
                ],
              ),
              Spacer(),
              // widget,
              Row(
                children: [
                  SizedBox(
                    width: 150,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.baseline,
                      mainAxisAlignment: MainAxisAlignment.center,
                      textBaseline: TextBaseline.alphabetic,
                      textDirection: TextDirection.rtl,
                      children: [
                        Text(
                          title,
                          locale: Locale('ar'),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          softWrap: false,
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                              textBaseline: TextBaseline.ideographic),
                          textDirection: TextDirection.rtl,
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          subtitle,
                          locale: Locale('ar'),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          textDirection: TextDirection.ltr,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: Color(0xff0B204C),
                              fontSize: 14,
                              textBaseline: TextBaseline.ideographic),
                        ),
                        SizedBox(
                          height: 1,
                        ),
                        Text(
                          adders,
                          locale: Locale('ar'),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: Colors.grey.shade600,
                              fontSize: 13,
                              textBaseline: TextBaseline.ideographic),
                          textDirection: TextDirection.rtl,
                          textAlign: TextAlign.start,
                        ),


                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(width: 10,),
              Padding(
                padding: const EdgeInsets.only(top: 20, bottom: 0, left: 16),
                child: Container(
                  clipBehavior: Clip.antiAlias,
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100),
                  ),
                  child: url == null ? Icon(Icons.person) : Image.network(url, fit: BoxFit.cover,),
                ),
              ),
            ],
          ),
        ),
      ),
  );
}
Widget showItem({
  required BuildContext context,
  required String title,
  required String subtitle,
  String? url,
  required int index,
  required Widget widget1,
  required Widget widget2,
  // Locale  = Locale('ar'),
  required List<DocumentSnapshot> documents,
}) {
  return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 3.0, bottom: 3),
        child: Container(
          height: SizeConfig().scaleHeight(90),
          width: SizeConfig().scaleWidth(366),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(14),
          ),
          child: Row(
            children: [
              widget1,
              widget2,
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  textBaseline: TextBaseline.ideographic,
                  children: [
                    Text(
                      title,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                      style: TextStyle(
                          color: Color(0xff0B204C),
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                          textBaseline: TextBaseline.ideographic),
                      textDirection: TextDirection.rtl,
                      textAlign: TextAlign.start,
                      locale: Locale('ar'),
                    ),
                    Text(
                      subtitle,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: Color(0xff919BB3),
                          fontSize: 13,
                          textBaseline: TextBaseline.ideographic),
                    ),
                  ],
                ),
              ),
              SizedBox(width: 10,),
              Padding(
                padding: const EdgeInsets.only(top: 16, bottom: 16, left: 0),
                child: CircleAvatar(
                  backgroundImage: url != null ? NetworkImage(url) : null,
                  radius: 25,
                  child: url == null ? Icon(Icons.person) : null,
                  backgroundColor: Color(0xff5A55CA),
                ),
              ),
            ],
          ),
        ),
      ));
}