import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProfileSetting extends StatelessWidget {
  final String title;
  final IconData firstIcon;
  void Function() onPressed;

  ProfileSetting({
    required this.title,
    required this.firstIcon,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onPressed,
      leading: Icon(
        firstIcon,
        color: Colors.green,
        size: 25,
      ),
      title: Text(
        title,
        style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
      ),
      trailing: Icon(
        Icons.arrow_forward_ios,
        color: Colors.grey,
        size: 15,
      ),
    );
  }
}