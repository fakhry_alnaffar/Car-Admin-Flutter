import 'package:flutter/material.dart';

class DashboardItem {
  late String title;
  late var icon;
  late Color iconColor;

  DashboardItem(
      {required this.title,
        required this.icon,
        required this.iconColor});
}
