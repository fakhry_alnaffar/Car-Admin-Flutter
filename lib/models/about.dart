class About{
  String aboutApp = '';
  String address = '';
  String phone = '';
  String email = '';
  String path = '';

  About();

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['aboutApp'] = aboutApp;
    map['address'] = address;
    map['phone'] = phone;
    map['email'] = email;
    return map;
  }
}