import 'package:car_admin_project/models/about.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class FirebaseFirestoreController {
  FirebaseFirestore _firebaseFirestore = FirebaseFirestore.instance;

  Stream<QuerySnapshot> readOrder() async* {
    yield* _firebaseFirestore
        .collection('merchant')
        .where('accept', isEqualTo: '0')
        .snapshots();
  }

  Stream<QuerySnapshot> readUsers() async* {
    yield* _firebaseFirestore
        .collection('user')
        .where('isAdmin', isEqualTo: '0')
        .snapshots();
  }

  Future<String> getBlockStateMerchant(String email) async {
    return await _firebaseFirestore
        .collection('merchant')
        .where('email', isEqualTo: email)
        .get()
        .then((value) {
      if (value.docs.length > 0) {
        return value.docs[0].get('isBlock');
      } else {
        return 'للاسف، تم حظر حسابك';
      }
    });
  }

  Future<String> getBlockStateUser2(String email) async {
    return await _firebaseFirestore
        .collection('user')
        .where('email', isEqualTo: email)
        .get()
        .then((value) {
      if (value.docs.length > 0) {
        return value.docs[0].get('isBlock');
      } else {
        return 'للاسف، تم حظر حسابك';
      }
    });
  }

  Stream<QuerySnapshot> readMerchant() async* {
    yield* _firebaseFirestore.collection('merchant').snapshots();
  }

  Stream<QuerySnapshot> readAllUser() async* {
    yield* _firebaseFirestore.collection('user').snapshots();
  }

  Stream<QuerySnapshot> readAdmins() async* {
    yield* _firebaseFirestore
        .collection('user')
        .where('isAdmin', isEqualTo: '1')
        .snapshots();
  }

  Stream<QuerySnapshot> readAdmin() async* {
    yield* _firebaseFirestore
        .collection('user')
        .where('isAdmin', isEqualTo: '1')
        .snapshots();
  }

  Future<bool> acceptOrder(String path) async {
    return await _firebaseFirestore
        .collection('merchant')
        .doc(path)
        .update({
          'accept': '1',
          'date':
              '${DateTime.now().year} / ${DateTime.now().month + 1} / ${DateTime.now().day}',
          'created': DateTime.now().toString().substring(0, 10)
        })
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> addAdmin(String path) async {
    return await _firebaseFirestore
        .collection('user')
        .doc(path)
        .update({'isAdmin': '1'})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> addUser(String path) async {
    return await _firebaseFirestore
        .collection('user')
        .doc(path)
        .update({'isAdmin': '0'})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> deleteMerchant({required String path}) async {
    return await _firebaseFirestore
        .collection('merchant')
        .doc(path)
        .delete()
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<String> getUserType(String email) async {
    return _firebaseFirestore
        .collection('user')
        .where('email', isEqualTo: email)
        .get()
        .then((value) {
      if (value.docs.length > 0) {
        return value.docs[0].get('isAdmin');
      } else {
        return '-1';
      }
    });
  }

  // Future<String> getType() async {
  //   return _firebaseFirestore
  //       .collection('active')
  //       .where('active_app', isEqualTo: '1')
  //       .get()
  //       .then((value) {
  //     if (value.docs.length > 0) {
  //       return value.docs[0].get('isAdmin');
  //     } else {
  //       return '-1';
  //     }
  //   });
  // }
  Future<String> getActive() async {
    return await _firebaseFirestore
        .collection('active')
        .get()
        .then((value) => value.docs[0].get('active_app'));
  }

  Future<DocumentSnapshot> getAdmin() async {
    return await _firebaseFirestore
        .collection('user')
        .where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email)
        .get()
        .then((value) => value.docs[0]);
  }

  Future<DocumentSnapshot> getSubscription() async {
    return await _firebaseFirestore
        .collection('subscription')
        .get()
        .then((value) => value.docs[0]);
  }

  Future<DocumentSnapshot> getAbout() async {
    return await _firebaseFirestore
        .collection('about')
        .get()
        .then((value) => value.docs[0]);
  }

  Future<DocumentSnapshot> getUsing() async {
    return await _firebaseFirestore
        .collection('using')
        .get()
        .then((value) => value.docs[0]);
  }

  Future<bool> updateUsing(
      {required String? path, required String using}) async {
    return await _firebaseFirestore
        .collection('using')
        .doc(path)
        .update({'using': using})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateUserName(
      {required String? path, required String name}) async {
    return await _firebaseFirestore
        .collection('user')
        .doc(path)
        .update({'name': name})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateUserImage(
      {required String? path, required String image}) async {
    return await _firebaseFirestore
        .collection('user')
        .doc(path)
        .update({'image': image})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateUserPhone(
      {required String? path, required String phone}) async {
    return await _firebaseFirestore
        .collection('user')
        .doc(path)
        .update({'phoneNumber': phone})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateUserPlace(
      {required String? path, required String place}) async {
    return await _firebaseFirestore
        .collection('user')
        .doc(path)
        .update({'place': place})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<String> getName() async {
    return _firebaseFirestore
        .collection('user')
        .where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email)
        .get()
        .then((value) => value.docs[0].get('name'));
  }

  Future<String> getUserImage() async {
    return _firebaseFirestore
        .collection('user')
        .where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email)
        .get()
        .then((value) => value.docs[0].get('image'));
  }

  Future<bool> updateAbout(
      {required String? path, required About about}) async {
    return await _firebaseFirestore
        .collection('about')
        .doc(path)
        .update(about.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateAboutName(
      {required String? path, required String aboutApp}) async {
    return await _firebaseFirestore
        .collection('about')
        .doc(path)
        .update({'aboutApp': aboutApp})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateAboutEmail(
      {required String? path, required String email}) async {
    return await _firebaseFirestore
        .collection('about')
        .doc(path)
        .update({'email': email})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateAboutAddress(
      {required String? path, required String address}) async {
    return await _firebaseFirestore
        .collection('about')
        .doc(path)
        .update({'address': address})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateAboutPhone(
      {required String? path, required String phone}) async {
    return await _firebaseFirestore
        .collection('about')
        .doc(path)
        .update({'phone': phone})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateAdsMainPrice(
      {required String? path, required String main}) async {
    return await _firebaseFirestore
        .collection('subscription')
        .doc(path)
        .update({'main': main})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateAdsDefPrice(
      {required String? path, required String defaulte}) async {
    return await _firebaseFirestore
        .collection('subscription')
        .doc(path)
        .update({'default': defaulte})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateSubUser(
      {required String? path, required String user}) async {
    return await _firebaseFirestore
        .collection('subscription')
        .doc(path)
        .update({'user': user})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateSubManager(
      {required String? path, required String manager}) async {
    return await _firebaseFirestore
        .collection('subscription')
        .doc(path)
        .update({'manager': manager})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> blockUser(String id) async {
    return await _firebaseFirestore
        .collection('block')
        .add({'id': id})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateToBlock({required String? path}) async {
    return await _firebaseFirestore
        .collection('user')
        .doc(path)
        .update({'isBlock': '1'})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateToBlockMerchant({required String? path}) async {
    return await _firebaseFirestore
        .collection('merchant')
        .doc(path)
        .update({'isBlock': '1'})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateToActive({required String? path}) async {
    return await _firebaseFirestore
        .collection('user')
        .doc(path)
        .update({'isBlock': '0'})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateToActiveMerchant({required String? path}) async {
    return await _firebaseFirestore
        .collection('merchant')
        .doc(path)
        .update({'isBlock': '0'})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> setUserAvailable({required String path}) async {
    return await _firebaseFirestore
        .collection('block')
        .doc(path)
        .delete()
        .then((value) => true)
        .catchError((error) => false);
  }

  Stream<QuerySnapshot> readBlockUser() async* {
    yield* _firebaseFirestore
        .collection('user')
        .where('isBlock', isEqualTo: '1')
        .snapshots();
  }

  Stream<QuerySnapshot> readAvailableUser(List<String> ids) async* {
    yield* _firebaseFirestore
        .collection('user')
        .where('isBlock', isEqualTo: '0')
        .where(FieldPath.documentId, whereNotIn: ids.length == 0 ? ['1'] : ids)
        .snapshots();
  }

  Stream<QuerySnapshot> readBlockMerchant2() async* {
    yield* _firebaseFirestore
        .collection('merchant')
        .where('isBlock', isEqualTo: '1')
        .snapshots();
  }

  Stream<QuerySnapshot> readBlockMerchant3() async* {
    yield* _firebaseFirestore
        .collection('merchant')
        .where('isBlock', isEqualTo: '0')
        .snapshots();
  }

  Stream<QuerySnapshot> readAvailableMerchant2(List<String> ids) async* {
    yield* _firebaseFirestore
        .collection('merchant')
        .where('isBlock', isEqualTo: '0')
        .where(FieldPath.documentId, whereNotIn: ids.length == 0 ? ['1'] : ids)
        .snapshots();
  }

  Future<List<String>> getBlockIds() async {
    List<String> list = <String>[];
    return await _firebaseFirestore.collection('block').get().then((value) {
      for (int i = 0; i < value.docs.length; i++) {
        list.add(value.docs[i].get('id'));
      }
      return list;
    });
  }

  Stream<QuerySnapshot> readAvailableMerchant(List<String> date) async* {
    yield* _firebaseFirestore
        .collection('merchant')
        .where('date', whereNotIn: date.length == 0 ? ['1'] : date)
        .snapshots();
  }

  // .where('isBlock', isEqualTo: '0')
  Stream<QuerySnapshot> readBlockMerchant(List<String> date) async* {
    yield* _firebaseFirestore
        .collection('merchant')
        .where('date', whereIn: date.length == 0 ? ['1'] : date)
        .snapshots();
  }

  Future<List<String>> getMerchantDate() async {
    List<String> list = <String>[];
    return await _firebaseFirestore.collection('merchant').get().then((value) {
      for (int i = 0; i < value.docs.length; i++) {
        int year = value.docs[i].get('date').toString().indexOf('/');
        int month = value.docs[i].get('date').toString().indexOf('-');
        if (DateTime.now().year ==
            int.parse(
                value.docs[i].get('date').toString().substring(0, year))) {
          if (DateTime.now().month >
              int.parse(value.docs[i]
                  .get('date')
                  .toString()
                  .substring(year + 1, month))) {
            list.add(value.docs[i].get('date'));
          }
          if (DateTime.now().month ==
              int.parse(value.docs[i]
                  .get('date')
                  .toString()
                  .substring(year + 1, month))) {
            if (DateTime.now().day >
                int.parse(value.docs[i]
                    .get('date')
                    .toString()
                    .substring(month + 1))) {
              list.add(value.docs[i].get('date'));
            }
          }
        }
      }
      return list;
    });
  }

  Future<bool> newSub(String path) async {
    return await _firebaseFirestore
        .collection('merchant')
        .doc(path)
        .update({
          'date':
              '${DateTime.now().year}/${DateTime.now().month + 1}-${DateTime.now().day}'
        })
        .then((value) => true)
        .catchError((error) => false);
  }

  Stream<QuerySnapshot> searchBlockMerchant(
      List<String> date, String name) async* {
    yield* _firebaseFirestore
        .collection('merchant')
        .where('marketName', isEqualTo: name)
        .where('isBlock', isEqualTo: '1')
        .snapshots();
  }

  Stream<QuerySnapshot> searchActive(List<String> date, String name) async* {
    yield* _firebaseFirestore
        .collection('merchant')
        .where('isBlock', isEqualTo: '0')
        .where('marketName', isEqualTo: name)
        .snapshots();
  }

  Stream<QuerySnapshot> searchAvailableMerchant(
      List<String> date, String name) async* {
    yield* _firebaseFirestore
        .collection('merchant')
        .where('date', whereNotIn: date.length == 0 ? ['1'] : date)
        .where('marketName', isEqualTo: name)
        .snapshots();
  }

  Stream<QuerySnapshot> searchBlockUser(List<String> ids, String name) async* {
    yield* _firebaseFirestore
        .collection('user')
        .where('isBlock', isEqualTo: '1')
        .where('name', isEqualTo: name)
        .snapshots();
  }

  Stream<QuerySnapshot> searchAvailableUser(
      List<String> ids, String name) async* {
    yield* _firebaseFirestore
        .collection('user')
        .where('isBlock', isEqualTo: '0')
        .where(FieldPath.documentId, whereNotIn: ids.length == 0 ? ['1'] : ids)
        .where('name', isEqualTo: name)
        .snapshots();
  }
}
