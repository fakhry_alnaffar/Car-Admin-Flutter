import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:uuid/uuid.dart';

class FirebaseStorageController {
  FirebaseStorage _firebaseStorage = FirebaseStorage.instance;

  Future<String> uploadImage(File file, String path) async{
    var uuid = Uuid();
    String name = '$path/${uuid.v4()}';
    var image = _firebaseStorage.ref(name);
    await image.putFile(file);
    return await image.getDownloadURL();
  }

}
