import 'package:car_admin_project/preferences/student_pref.dart';
import 'package:car_admin_project/screens/customer/customer_screen.dart';
import 'package:car_admin_project/screens/home/home_screen.dart';
import 'package:car_admin_project/screens/launch/launch_screen.dart';
import 'package:car_admin_project/screens/login/login_screen.dart';
import 'package:car_admin_project/screens/manger/add_admin.dart';
import 'package:car_admin_project/screens/manger/admins.dart';
import 'package:car_admin_project/screens/manger/manger_screen.dart';
import 'package:car_admin_project/screens/manger/setting_screen_cont.dart';
import 'package:car_admin_project/screens/setting/change_password.dart';
import 'package:car_admin_project/screens/setting/profile_screen.dart';
import 'package:car_admin_project/screens/setting/setting_screen_controller.dart';
import 'package:car_admin_project/screens/manger/subscription.dart';
import 'package:car_admin_project/screens/manger/user_agreement.dart';
import 'package:car_admin_project/screens/merchant/merchant_screen.dart';
import 'package:car_admin_project/screens/order/order.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await AppPreferences().initPreferences();
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}
class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    Locale _locale = Locale('ar');

    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        Locale('ar'),
      ],
      debugShowCheckedModeBanner: false,
      initialRoute: 'launch_screen',

      //routes
      routes: {
        'launch_screen': (context) => LaunchScreen(),
        'login_screen': (context) => LoginScreen(),
        'home_screen': (context) => HomeScreen(),
        'order_screen': (context) => OrderScreen(),
        'merchant_screen': (context) => MerchantScreen(),
        'customer_screen': (context) => CustomerScreen(),
        'manger_screen': (context) => MangerScreen(),
        // 'setting_controller_screen': (context) => SettingScreenController(),
        // 'user_agreement': (context) => UserAgreement(),
        // 'subscription_agreement': (context) => SubscriptionScreen(),
        'add_admin_screen': (context) => AddAdmin(),
        // 'setting_screen_controller': (context) => SettingScreenController(),
        'read_admins': (context) => AdminsScreen(),
        'profile_screen': (context) => ProfileScreen(),
        'change_password_screen': (context) => ChangePasswordScreen(),
      },
    );
  }
}

