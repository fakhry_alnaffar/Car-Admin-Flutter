import 'package:car_admin_project/firebase/firebase_firestor_controller.dart';
import 'package:car_admin_project/responsive/size_config.dart';
import 'package:car_admin_project/widgets/component.dart';
import 'package:car_admin_project/widgets/loading.dart';
import 'package:car_admin_project/widgets/no_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class OrderScreen extends StatefulWidget {
  const OrderScreen({Key? key}) : super(key: key);

  @override
  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: Container(
          margin: EdgeInsetsDirectional.only(top: 10),
          child: Text(
            'الطلبات',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Color(0xff1DB854),
          ),
          onPressed: () {
            Navigator.pushReplacementNamed(context, 'home_screen');
          },
        ),
      ),
      body: StreamBuilder<QuerySnapshot>(
          stream: FirebaseFirestoreController().readOrder(),
          builder: (context, snapshot) {
            if (snapshot.hasData && snapshot.data!.docs.length != 0) {
              List<DocumentSnapshot> documents = snapshot.data!.docs;
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: ListView.builder(
                    physics: BouncingScrollPhysics(),
                    itemCount: documents.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        child: showItem(
                          index: index,
                          widget1: IconButton(
                              onPressed: () async {
                                showDialog(
                                    barrierColor: Colors.black.withOpacity(0.16),
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        backgroundColor: Color(0xff1DB854),
                                        clipBehavior: Clip.antiAlias,
                                        contentPadding:
                                        EdgeInsetsDirectional.zero,
                                        actionsPadding:
                                        EdgeInsetsDirectional.zero,
                                        title: Center(
                                            child: Text(
                                              'هل انت متاكد ؟',
                                              style: TextStyle(color: Colors.white),
                                            )),
                                        actions: [
                                          Container(
                                            alignment:
                                            AlignmentDirectional.bottomStart,
                                            margin: EdgeInsetsDirectional.only(
                                                start: 0,
                                                end: 0,
                                                bottom: 0,
                                                top: 0),
                                            padding: EdgeInsetsDirectional.zero,
                                            height: 50,
                                            color: Colors.white70,
                                            child: Stack(children: [
                                              Column(
                                                children: [
                                                  SizedBox(
                                                    height: 50,
                                                    child: Align(
                                                      alignment:
                                                      Alignment.bottomCenter,
                                                      child: Container(
                                                        alignment:
                                                        AlignmentDirectional
                                                            .centerEnd,
                                                        height: 50,
                                                        padding:
                                                        EdgeInsetsDirectional
                                                            .zero,
                                                        width: double.infinity,
                                                        color: Colors.transparent,
                                                        child: Row(children: [
                                                          Expanded(
                                                            child: TextButton(
                                                              onPressed:
                                                                  () async {
                                                                Navigator.pop(
                                                                    context);
                                                                await FirebaseFirestoreController()
                                                                    .deleteMerchant(path: documents[index].id);                                                          },
                                                              child: Text(
                                                                'نعم',
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                    fontSize: 18),
                                                              ),
                                                              style:
                                                              ElevatedButton
                                                                  .styleFrom(
                                                                minimumSize: Size(
                                                                    double
                                                                        .infinity,
                                                                    SizeConfig()
                                                                        .scaleHeight(
                                                                        60)),
                                                                primary: Color(
                                                                    0xff1DB854),
                                                                shape:
                                                                RoundedRectangleBorder(
                                                                  borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                      0),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            width: 1,
                                                          ),
                                                          Expanded(
                                                            child: TextButton(
                                                              onPressed: () {
                                                                Navigator.pop(
                                                                    context);
                                                              },
                                                              child: Text(
                                                                'لا',
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontSize: 18,
                                                                    fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                              ),
                                                              style:
                                                              ElevatedButton
                                                                  .styleFrom(
                                                                minimumSize: Size(
                                                                    double
                                                                        .infinity,
                                                                    SizeConfig()
                                                                        .scaleHeight(
                                                                        60)),
                                                                primary: Color(
                                                                    0xff1DB854),
                                                                shape:
                                                                RoundedRectangleBorder(
                                                                  borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                      0),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ]),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ]),
                                          ),
                                        ],
// actionsPadding: EdgeInsets.symmetric(horizontal: 50),
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(15),
                                        ),
                                      );
                                    });
                              },
                              icon: Icon(
                                Icons.cancel,
                                color: Color(0xffec0606),
                                size: 25,
                              )),
                          widget2: IconButton(
                              onPressed: () async {
                                showDialog(
                                    barrierColor: Colors.black.withOpacity(0.16),
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        backgroundColor: Color(0xff1DB854),
                                        clipBehavior: Clip.antiAlias,
                                        contentPadding:
                                        EdgeInsetsDirectional.zero,
                                        actionsPadding:
                                        EdgeInsetsDirectional.zero,
                                        title: Center(
                                            child: Text(
                                              'هل انت متاكد ؟',
                                              style: TextStyle(color: Colors.white),
                                            )),
                                        actions: [
                                          Container(
                                            alignment:
                                            AlignmentDirectional.bottomStart,
                                            margin: EdgeInsetsDirectional.only(
                                                start: 0,
                                                end: 0,
                                                bottom: 0,
                                                top: 0),
                                            padding: EdgeInsetsDirectional.zero,
                                            height: 50,
                                            color: Colors.white70,
                                            child: Stack(children: [
                                              Column(
                                                children: [
                                                  SizedBox(
                                                    height: 50,
                                                    child: Align(
                                                      alignment:
                                                      Alignment.bottomCenter,
                                                      child: Container(
                                                        alignment:
                                                        AlignmentDirectional
                                                            .centerEnd,
                                                        height: 50,
                                                        padding:
                                                        EdgeInsetsDirectional
                                                            .zero,
                                                        width: double.infinity,
                                                        color: Colors.transparent,
                                                        child: Row(children: [
                                                          Expanded(
                                                            child: TextButton(
                                                              onPressed:
                                                                  () async {
                                                                Navigator.pop(
                                                                    context);
                                                                await FirebaseFirestoreController()
                                                                    .acceptOrder(documents[index].id);                                                              },
                                                              child: Text(
                                                                'نعم',
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                    fontSize: 18),
                                                              ),
                                                              style:
                                                              ElevatedButton
                                                                  .styleFrom(
                                                                minimumSize: Size(
                                                                    double
                                                                        .infinity,
                                                                    SizeConfig()
                                                                        .scaleHeight(
                                                                        60)),
                                                                primary: Color(
                                                                    0xff1DB854),
                                                                shape:
                                                                RoundedRectangleBorder(
                                                                  borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                      0),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            width: 1,
                                                          ),
                                                          Expanded(
                                                            child: TextButton(
                                                              onPressed: () {
                                                                Navigator.pop(
                                                                    context);
                                                              },
                                                              child: Text(
                                                                'لا',
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontSize: 18,
                                                                    fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                              ),
                                                              style:
                                                              ElevatedButton
                                                                  .styleFrom(
                                                                minimumSize: Size(
                                                                    double
                                                                        .infinity,
                                                                    SizeConfig()
                                                                        .scaleHeight(
                                                                        60)),
                                                                primary: Color(
                                                                    0xff1DB854),
                                                                shape:
                                                                RoundedRectangleBorder(
                                                                  borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                      0),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ]),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ]),
                                          ),
                                        ],
// actionsPadding: EdgeInsets.symmetric(horizontal: 50),
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(15),
                                        ),
                                      );
                                    });
                              },
                              icon: Icon(
                                Icons.check_circle,
                                color: Color(0xff1DB854),
                                size: 25,
                              )),
                          context: context,
                          title: documents[index].get('marketName'),
                          subtitle: documents[index].get('phoneNumber'),
                          url: documents[index].get('image'), documents: documents,
                        ),
                        onTap: () {
                          print(index);
                        },
                      );
                    }),
              );
            } else if (snapshot.connectionState == ConnectionState.waiting) {
              return Loading();
            } else {
              return NoData();
            }
          }),
    );
  }
}
