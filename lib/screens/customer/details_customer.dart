import 'package:car_admin_project/firebase/firebase_firestor_controller.dart';
import 'package:car_admin_project/responsive/size_config.dart';
import 'package:car_admin_project/widgets/component.dart';
import 'package:car_admin_project/widgets/loading.dart';
import 'package:car_admin_project/widgets/no_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class DetailsCustomer extends StatefulWidget {
  const DetailsCustomer({Key? key}) : super(key: key);

  @override
  _DetailsCustomerState createState() => _DetailsCustomerState();
}

class _DetailsCustomerState extends State<DetailsCustomer> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: Container(
          margin: EdgeInsetsDirectional.only(top: 10),
          child: Text(
            'الاعلانات المستحقة',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Color(0xff1DB854),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: StreamBuilder<QuerySnapshot>(
                  stream: FirebaseFirestoreController().readAdmins(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData && snapshot.data!.docs.length > 0) {
                      List<DocumentSnapshot> documents = snapshot.data!.docs;
                      return Container(
                        margin: EdgeInsetsDirectional.zero,
                        padding: EdgeInsetsDirectional.zero,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 24),
                          child: ListView.builder(
                              itemCount: documents.length,
                              physics: BouncingScrollPhysics(),
                              padding: EdgeInsetsDirectional.zero,
                              itemBuilder: (context, index) {
                                return InkWell(
                                  child: showItem(
                                    widget1: IconButton(
                                        padding: EdgeInsetsDirectional.zero,
                                        onPressed: () async {
                                          showDialog(
                                              barrierColor: Colors.black
                                                  .withOpacity(0.16),
                                              context: context,
                                              builder: (context) {
                                                return AlertDialog(
                                                  backgroundColor:
                                                      Color(0xff1DB854),
                                                  clipBehavior: Clip.antiAlias,
                                                  contentPadding:
                                                      EdgeInsetsDirectional
                                                          .zero,
                                                  actionsPadding:
                                                      EdgeInsetsDirectional
                                                          .zero,
                                                  title: Center(
                                                      child: Text(
                                                    'هل انت متاكد ؟',
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  )),
                                                  actions: [
                                                    Container(
                                                      alignment:
                                                          AlignmentDirectional
                                                              .bottomStart,
                                                      margin:
                                                          EdgeInsetsDirectional
                                                              .only(
                                                                  start: 0,
                                                                  end: 0,
                                                                  bottom: 0,
                                                                  top: 0),
                                                      padding:
                                                          EdgeInsetsDirectional
                                                              .zero,
                                                      height: 50,
                                                      color: Colors.white70,
                                                      child: Stack(children: [
                                                        Column(
                                                          children: [
                                                            SizedBox(
                                                              height: 50,
                                                              child: Align(
                                                                alignment: Alignment
                                                                    .bottomCenter,
                                                                child:
                                                                    Container(
                                                                  alignment:
                                                                      AlignmentDirectional
                                                                          .centerEnd,
                                                                  height: 50,
                                                                  padding:
                                                                      EdgeInsetsDirectional
                                                                          .zero,
                                                                  width: double
                                                                      .infinity,
                                                                  color: Colors
                                                                      .transparent,
                                                                  child: Row(
                                                                      children: [
                                                                        Expanded(
                                                                          child:
                                                                              TextButton(
                                                                            onPressed:
                                                                                () async {
                                                                              Navigator.pop(context);
                                                                            },
                                                                            child:
                                                                                Text(
                                                                              'نعم',
                                                                              style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18),
                                                                            ),
                                                                            style:
                                                                                ElevatedButton.styleFrom(
                                                                              minimumSize: Size(double.infinity, SizeConfig().scaleHeight(60)),
                                                                              primary: Color(0xff1DB854),
                                                                              shape: RoundedRectangleBorder(
                                                                                borderRadius: BorderRadius.circular(0),
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        SizedBox(
                                                                          width:
                                                                              1,
                                                                        ),
                                                                        Expanded(
                                                                          child:
                                                                              TextButton(
                                                                            onPressed:
                                                                                () {
                                                                              Navigator.pop(context);
                                                                            },
                                                                            child:
                                                                                Text(
                                                                              'لا',
                                                                              style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),
                                                                            ),
                                                                            style:
                                                                                ElevatedButton.styleFrom(
                                                                              minimumSize: Size(double.infinity, SizeConfig().scaleHeight(60)),
                                                                              primary: Color(0xff1DB854),
                                                                              shape: RoundedRectangleBorder(
                                                                                borderRadius: BorderRadius.circular(0),
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                      ]),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ]),
                                                    ),
                                                  ],
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15),
                                                  ),
                                                );
                                              });
                                        },
                                        icon: Icon(
                                          Icons.check_circle_outline_rounded,
                                          color: Color(0xff1DB854),
                                          size: 25,
                                        )),
                                    widget2: IconButton(
                                        padding: EdgeInsetsDirectional.zero,
                                        onPressed: () {},
                                        icon: Icon(
                                          Icons.call_rounded,
                                          color: Colors.transparent,
                                          size: 25,
                                        )),
                                    documents: documents,
                                    index: index,
                                    context: context,
                                    title: documents[index].get('name'),
                                    subtitle: documents[index]
                                        .get('place')
                                        .toString(),
                                    url: documents[index].get('image'),
                                  ),
                                  onTap: () {
                                    print(index);
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                DetailsCustomer()));
                                  },
                                );
                              }),
                        ),
                      );
                    } else if (snapshot.connectionState ==
                        ConnectionState.waiting) {
                      return Loading();
                    } else {
                      return NoData();
                    }
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
