import 'dart:io';

import 'package:car_admin_project/firebase/firebase_firestor_controller.dart';
import 'package:car_admin_project/firebase/firebase_storage.dart';
import 'package:car_admin_project/responsive/size_config.dart';
import 'package:car_admin_project/widgets/app_text_filed_profile.dart';
import 'package:car_admin_project/widgets/loading.dart';
import 'package:car_admin_project/widgets/no_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:future_progress_dialog/future_progress_dialog.dart';
import 'package:image_picker/image_picker.dart';
class SettingScreen extends StatefulWidget {
 final DocumentSnapshot snapshot;

  SettingScreen(this.snapshot);

  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  bool isAddersEdit = false;
  bool isEmailEdit = false;
  bool isPhoneEdit = false;
  bool isNameEdit = false;
  bool isShopNameEdit = false;
  bool isCountryEdit = false;
  bool isImageEdit = false;
  XFile? pickedImage;
  XFile? pickedImage2;
  ImagePicker imagePicker = ImagePicker();
  String selectedImage = '';
  IconData iconEditImage = Icons.edit;
  late TextEditingController _name;
  late TextEditingController _country;
  late TextEditingController _email;
  late TextEditingController _phone;
@override
  void initState() {
    // TODO: implement initState
    super.initState();
    _phone = TextEditingController(text: widget.snapshot.get('phoneNumber'));
    _country = TextEditingController(text: widget.snapshot.get('place'));
    _email = TextEditingController(text: widget.snapshot.get('email'));
    _name = TextEditingController(text: widget.snapshot.get('name'));
  }
  @override
  void dispose() {
  // TODO: implement dispose
    _phone.dispose();
    _email.dispose();
    _name.dispose();
    _country.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      backgroundColor: Color(0xffF1F2F3),
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.green),
        title: Container(
          margin: EdgeInsetsDirectional.only(top: 10),
          child: Text(
            'الصفحة الشخصية',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          clipBehavior: Clip.antiAlias,
          physics: BouncingScrollPhysics(),
          child: StreamBuilder<QuerySnapshot>(
              stream: FirebaseFirestoreController().readAdmin(),
              builder: (context, snapshot) {
                if(snapshot.hasData && snapshot.data!.docs.length > 0){
                  List<DocumentSnapshot> documents = snapshot.data!.docs;
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SizedBox(height: 20),
                      Center(
                        child: Stack(
                          alignment: AlignmentDirectional.bottomEnd,
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(80),
                                border:
                                Border.all(width: 8, color: Colors.white),
                              ),
                              child: Container(
                                clipBehavior: Clip.antiAlias,
                                width: 80,
                                height: 80,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(50),
                                    color: Colors.deepOrange),
                                child: widget.snapshot != null
                                    ? pickedImage2 != null
                                    ? Image.file(
                                  File(pickedImage2!.path),
                                  fit: BoxFit.cover,
                                )
                                    : Image.network(
                                  widget.snapshot.get('image'),
                                  fit: BoxFit.cover,
                                )
                                    : pickedImage2 != null
                                    ? Image.file(
                                  File(pickedImage2!.path),
                                  fit: BoxFit.cover,
                                )
                                    : Icon(
                                  Icons.add_photo_alternate_outlined,
                                  color: Colors.white,
                                  size: 50,
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsetsDirectional.only(
                                  bottom: 12, end: 5, top: 0, start: 0),
                              child: Container(
                                // padding: EdgeInsetsDirectional.only(bottom: 50),
                                child: CircleAvatar(
                                  backgroundColor: Colors.green,

                                  radius: 13,
                                  child: IconButton(
                                    padding: EdgeInsetsDirectional.zero,
                                    onPressed: () {
                                      isImageEdit = !isImageEdit;
                                      if (isImageEdit == true) {
                                        setState(() {
                                          pickImage();
                                          iconEditImage =
                                              Icons.check_outlined;
                                          print('go to check');
                                        });
                                      }
                                      if (isImageEdit == false) {
                                        setState(() {
                                          print('back to edit');
                                          iconEditImage = Icons.edit;
                                          ///method update image
                                          updateUserImage(documents[0].id);
                                        });
                                      }
                                    },
                                    icon: Icon(iconEditImage,color: Colors.white,size: 18,),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: SizeConfig().scaleHeight(20),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 32),
                        child: Column(
                          children: [
                            AppTextFiledProfile(
                              textInputType: TextInputType.text,
                              labelText: '',
                              readOnly: !isNameEdit,
                              showCursor: isNameEdit,
                              controller: _name,
                              prefix: Icons.person,
                              suffix: isNameEdit ? Icons.check : Icons.edit,
                              functionSuffixPressed: () {
                                setState(() {
                                  isNameEdit = !isNameEdit;
                                  if (isNameEdit == false) {
                                    updateUserName(documents[0].id);
                                  }
                                });
                              },
                            ),
                            SizedBox(
                              height: SizeConfig().scaleHeight(10),
                            ),
                            AppTextFiledProfile(
                              textInputType: TextInputType.text,
                              labelText: '',
                              readOnly: true,
                              showCursor: isCountryEdit,
                              controller: _country,
                              prefix: Icons.location_on_outlined,
                              suffix: isCountryEdit ? Icons.check : Icons.edit,
                              functionSuffixPressed: () {
                                setState(() {
                                  isCountryEdit = !isCountryEdit;
                                  if (isCountryEdit == true) {
                                    showBottomSheet();
                                  }
                                  if (isCountryEdit == false) {
                                    updateUserPlace(documents[0].id);
                                  }
                                });
                              },
                            ),
                            SizedBox(
                              height: SizeConfig().scaleHeight(10),
                            ),
                            AppTextFiledProfile(
                              textInputType: TextInputType.text,
                              labelText: '',
                              readOnly: !isEmailEdit,
                              showCursor: isEmailEdit,
                              controller: _email,
                              prefix: Icons.email_outlined,
                              suffix: isEmailEdit ? Icons.check : Icons.edit,
                              functionSuffixPressed: () {
                                // setState(() {
                                //   isNameEdit = !isNameEdit;
                                //   if (isNameEdit == true) {
                                //     ///update name method
                                //   }
                                // });
                              },
                            ),
                            SizedBox(
                              height: SizeConfig().scaleHeight(10),
                            ),
                            AppTextFiledProfile(
                              textInputType: TextInputType.text,
                              labelText: '',
                              readOnly: !isPhoneEdit,
                              showCursor: isPhoneEdit,
                              controller: _phone,
                              prefix: Icons.phone_android_rounded,
                              suffix: isPhoneEdit ? Icons.check : Icons.edit,
                              functionSuffixPressed: () {
                                setState(() {
                                  isPhoneEdit = !isPhoneEdit;
                                  if (isPhoneEdit == false) {
                                    updateUserPhone(documents[0].id);
                                  }
                                });
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  );
                }else if(snapshot.connectionState == ConnectionState.waiting){
                  return Loading();
                }else{
                  return NoData();
                }

              }
          ),
        ),
      ),
    );
  }

  Future<void> pickImage() async {
    pickedImage = await imagePicker.pickImage(source: ImageSource.gallery);
    if (pickedImage != null) {
      showDialog(
        context: context,
        builder: (context) => FutureProgressDialog(uploadImage(),
            message: Text('جاري تحميل الصورة...')),
      );
      setState(() {
        pickedImage2 = pickedImage;
      });
    }
  }

  Future<void> uploadImage() async {
    selectedImage = await FirebaseStorageController().uploadImage(File(pickedImage!.path), 'car');
  }

  Future<void> updateUserName(String path) async{
    await FirebaseFirestoreController().updateUserName(path: path, name: _name.text);
  }

  Future<void> updateUserImage(String path) async{
    await FirebaseFirestoreController().updateUserImage(path: path, image: selectedImage);
  }

  Future<void> updateUserPhone(String path) async{
    await FirebaseFirestoreController().updateUserPhone(path: path, phone: _phone.text);
  }

  Future<void> updateUserPlace(String path) async{
    await FirebaseFirestoreController().updateUserPlace(path: path, place: _country.text);
  }

  void showBottomSheet() {
    showModalBottomSheet(
      barrierColor: Colors.black.withOpacity(0.25),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      context: context,
      builder: (context) {
        return Padding(
          padding: EdgeInsets.symmetric(vertical: 15),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              RadioListTile(
                  title: Text('شمال غزة'),
                  value: 'شمال غزة',
                  groupValue: _country.text,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        _country.text = value;
                      });

                    Navigator.pop(context);
                  }),
              RadioListTile(
                  title: Text('غرب غزة'),
                  value: 'غرب غزة',
                  groupValue: _country.text,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        _country.text = value;
                      });

                    Navigator.pop(context);
                  }),
              RadioListTile(
                  title: Text('شرق غزة'),
                  value: 'شرق غزة',
                  groupValue: _country.text,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        _country.text = value;
                      });

                    Navigator.pop(context);
                  }),
              RadioListTile(
                  title: Text('جنوب غزة'),
                  value: 'جنوب غزة',
                  groupValue: _country.text,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        _country.text = value;
                      });
                    Navigator.pop(context);
                  }),
            ],
          ),
        );
      },
    );
  }

}
