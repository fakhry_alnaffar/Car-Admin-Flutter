import 'package:car_admin_project/firebase/firebase_firestor_controller.dart';
import 'package:car_admin_project/screens/setting/setting_screen_controller.dart';
import 'package:car_admin_project/widgets/loading.dart';
import 'package:car_admin_project/widgets/no_data.dart';
import 'package:car_admin_project/widgets/profile_item.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  String type = '';
  String _language = 'ar';
  late String name = '';
  DocumentSnapshot? adminSnapshot;

  @override
  void initState() {
    super.initState();
    getAdmin();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          'الصفحة الشخصية',
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: StreamBuilder<QuerySnapshot>(
              stream: FirebaseFirestoreController().readAdmin(),
              builder: (context, snapshot) {
                if (snapshot.hasData && snapshot.data!.docs.isNotEmpty) {
                  List<DocumentSnapshot> documents = snapshot.data!.docs;
                  return Column(
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        clipBehavior: Clip.antiAlias,
                        width: 100,
                        height: 100,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(200),
                        ),
                        child: Stack(
                          children: [
                            Container(
                              width: 100,
                              height: 100,
                              child: Image.network(documents[0].get('image'), fit: BoxFit.cover,),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        documents[0].get('name'),
                        style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      Text(
                        documents[0].get('email'),
                        style: TextStyle(fontSize: 15, color: Colors.grey),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        child: Column(
                          children: [
                            ProfileSetting(
                              title: 'تعديل الحساب',
                              firstIcon: Icons.person_outline,
                              onPressed: () {
                                Navigator.push(context, MaterialPageRoute(builder: (context) => SettingScreen(adminSnapshot!),));
                              },
                            ),
                            ProfileSetting(
                              title: 'تغيير كلمة السر',
                              firstIcon: Icons.lock_outline,
                              onPressed: () {
                                Navigator.pushNamed(context, 'change_password_screen');
                              },
                            ),
                            ProfileSetting(
                              title: 'تسجيل الخروج',
                              firstIcon: Icons.logout,
                              onPressed: () {
                                signOut();
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  );
                }else if(snapshot.connectionState == ConnectionState.waiting){
                  return Loading();
                }else{
                  return NoData();
                }
              }
          ),
        ),
      ),
    );
  }

  void showAlertDialog() {
    showDialog(
      context: context,
      barrierColor: Colors.black.withOpacity(0.25),
      builder: (context) {
        return AlertDialog(
          title: Text('Log out'),
          content: Text(
            'Are you sure you want to logout',
            style: TextStyle(color: Colors.grey),
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          actions: [
            TextButton(
              onPressed: () {
                signOut();
              },
              child: Text(
                'OK',
                style: TextStyle(color: Colors.deepOrange),
              ),
            ),
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text(
                'Cancel',
                style: TextStyle(color: Colors.grey),
              ),
            ),
          ],
        );
      },
    );
  }

  void showBottomSheet() {
    showModalBottomSheet(
      barrierColor: Colors.black.withOpacity(0.25),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      context: context,
      builder: (context) {
        return Padding(
          padding: EdgeInsets.symmetric(vertical: 15),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              RadioListTile(
                  title: Text('Arabic'),
                  value: 'ar',
                  groupValue: _language,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        _language = value;
                      });

                    Navigator.pop(context);
                  }),
              RadioListTile(
                  title: Text('English'),
                  value: 'en',
                  groupValue: _language,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        _language = value;
                      });

                    Navigator.pop(context);
                  }),
            ],
          ),
        );
      },
    );
  }

  Future<void> signOut() async {
    await FirebaseAuth.instance.signOut();
    Navigator.pushReplacementNamed(context, 'login_screen');
  }


// Users getUser(DocumentSnapshot snapshot){
//   Users users = Users();
//   users.email =snapshot.get('email');
//   users.image =snapshot.get('image');
//   users.name = snapshot.get('name');
//   return users;
// }
  Future<void> getAdmin() async{
    DocumentSnapshot snapshot = await FirebaseFirestoreController().getAdmin();
    setState(() {
      adminSnapshot = snapshot;
    });
  }
}

class ProfileIcon extends StatelessWidget {
  final String title;
  final IconData icon;

  ProfileIcon({
    required this.title,
    required this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Icon(
          icon,
          color: Colors.deepOrange,
        ),
        SizedBox(height: 10),
        Text(title),
      ],
    );
  }

}
