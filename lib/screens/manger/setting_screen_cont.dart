import 'package:car_admin_project/firebase/firebase_firestor_controller.dart';
import 'package:car_admin_project/models/about.dart';
import 'package:car_admin_project/responsive/size_config.dart';
import 'package:car_admin_project/utils/helpers.dart';
import 'package:car_admin_project/widgets/app_text_filed_profile.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
class SettingScreenController extends StatefulWidget {
  DocumentSnapshot about;

  SettingScreenController(this.about);

  @override
  _SettingScreenControllerState createState() => _SettingScreenControllerState();
}

class _SettingScreenControllerState extends State<SettingScreenController> with Helpers {
  bool isAddersEdit = false;
  bool isEmailEdit = false;
  bool isPhoneEdit = false;
  bool isAboutEdit = false;
  late TextEditingController _about;
  late TextEditingController _adders;
  late TextEditingController _phone;
  late TextEditingController _email;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _about = TextEditingController(text: widget.about.get('aboutApp'));
    _adders = TextEditingController(text: widget.about.get('address'));
    _phone = TextEditingController(text: widget.about.get('phone'));
    _email = TextEditingController(text: widget.about.get('email'));
  }
  @override
  void dispose() {
    // TODO: implement dispose
    _about.dispose();
    _adders.dispose();
    _phone.dispose();
    _email.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: Container(
          margin: EdgeInsetsDirectional.only(top: 10),
          child: Text(
            'الاعدادات',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Color(0xff1DB854),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          // clipBehavior: Clip.antiAlias,
          physics: BouncingScrollPhysics(),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 50),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                AppTextFiledProfile(
                  textInputType: TextInputType.text,
                  labelText: 'عن التطبيق',
                  readOnly: !isAboutEdit,
                  showCursor: isAboutEdit,
                  controller: _about,
                  prefix: Icons.account_balance_outlined,
                  suffix: isAboutEdit ? Icons.check : Icons.edit,
                  functionSuffixPressed: () {
                    setState(() {
                      isAboutEdit = !isAboutEdit;
                      if (isAboutEdit == false) {
                        updateAboutName();
                      }
                    });
                  },
                ),
                SizedBox(height: 20,),
                AppTextFiledProfile(
                  textInputType: TextInputType.text,
                  labelText: 'العنوان',
                  readOnly: !isAddersEdit,
                  showCursor: isAddersEdit,
                  controller: _adders,
                  prefix: Icons.location_on_outlined,
                  suffix: isAddersEdit ? Icons.check : Icons.edit,
                  functionSuffixPressed: () {
                    setState(() {
                      isAddersEdit = !isAddersEdit;
                      if (isAddersEdit == false) {
                        updateAboutAddress();
                      }
                    });
                  },
                ),
                SizedBox(height: 20,),
                AppTextFiledProfile(
                  textInputType: TextInputType.text,
                  labelText: 'رقم الهاتف',
                  readOnly: !isPhoneEdit,
                  showCursor: isPhoneEdit,
                  controller: _phone,
                  prefix: Icons.phone,
                  suffix: isPhoneEdit ? Icons.check : Icons.edit,
                  functionSuffixPressed: () {
                    setState(() {
                      isPhoneEdit = !isPhoneEdit;
                      if (isPhoneEdit == false) {
                        updateAboutPhone();
                      }
                    });
                  },
                ),
                SizedBox(height: 20,),
                AppTextFiledProfile(
                  textInputType: TextInputType.text,
                  labelText: 'البريد الاكتروني',
                  readOnly: !isEmailEdit,
                  showCursor: isEmailEdit,
                  controller: _email,
                  prefix: Icons.email_rounded,
                  suffix: isEmailEdit ? Icons.check : Icons.edit,
                  functionSuffixPressed: () {
                    setState(() {
                      isEmailEdit = !isEmailEdit;
                      if (isEmailEdit == false) {
                        updateAboutEmail();
                      }
                    });
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> updateAboutName() async{
    if(_about.text.isNotEmpty)
    await FirebaseFirestoreController().updateAboutName(path: widget.about.id, aboutApp: _about.text);
  }

  Future<void> updateAboutEmail() async{
    if(_email.text.isNotEmpty)
    await FirebaseFirestoreController().updateAboutEmail(path: widget.about.id, email: _email.text);
  }

  Future<void> updateAboutPhone() async{
    if(_phone.text.isNotEmpty)
    await FirebaseFirestoreController().updateAboutPhone(path: widget.about.id, phone: _phone.text);
  }

  Future<void> updateAboutAddress() async{
    if(_adders.text.isNotEmpty)
    await FirebaseFirestoreController().updateAboutAddress(path: widget.about.id, address: _adders.text);
  }

}
