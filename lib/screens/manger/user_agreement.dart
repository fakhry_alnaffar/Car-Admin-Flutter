import 'package:car_admin_project/firebase/firebase_firestor_controller.dart';
import 'package:car_admin_project/responsive/size_config.dart';
import 'package:car_admin_project/widgets/app_text_filed_profile.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
class UserAgreement extends StatefulWidget {
  DocumentSnapshot using;

  UserAgreement(this.using);

  @override
  _UserAgreementState createState() => _UserAgreementState();
}

class _UserAgreementState extends State<UserAgreement> {
  bool isPhoneEdit = false;
  late TextEditingController _adders;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _adders = TextEditingController(text: widget.using.get('using'));

  }
  @override
  void dispose() {
    // TODO: implement dispose
    _adders.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: Container(
          margin: EdgeInsetsDirectional.only(top: 10),
          child: Text(
            'اتفاقية المستخدم',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Color(0xff1DB854),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          // clipBehavior: Clip.antiAlias,
          physics: BouncingScrollPhysics(),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 50),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                AppTextFiledProfile(
                  textInputType: TextInputType.text,
                  labelText: '',
                  readOnly: !isPhoneEdit,
                  showCursor: isPhoneEdit,
                  controller: _adders,
                  prefix: Icons.title,
                  suffix: isPhoneEdit ? Icons.check : Icons.edit,
                  functionSuffixPressed: () {
                    setState(() {
                      isPhoneEdit = !isPhoneEdit;
                      if (isPhoneEdit == false) {
                        updateUsing(widget.using.id);
                      }
                    });
                  },
                ),
            ]),
          ),
        ),
      ),
    );
  }

  Future<void> updateUsing(String path) async{
    await FirebaseFirestoreController().updateUsing(path: path, using: _adders.text);
  }

}
