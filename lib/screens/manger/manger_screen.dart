import 'package:car_admin_project/firebase/firebase_firestor_controller.dart';
import 'package:car_admin_project/responsive/size_config.dart';
import 'package:car_admin_project/screens/manger/setting_screen_cont.dart';
import 'package:car_admin_project/screens/manger/subscription.dart';
import 'package:car_admin_project/screens/setting/setting_screen_controller.dart';
import 'package:car_admin_project/screens/manger/user_agreement.dart';
import 'package:car_admin_project/widgets/component.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
class MangerScreen extends StatefulWidget {
  const MangerScreen({Key? key}) : super(key: key);

  @override
  _MangerScreenState createState() => _MangerScreenState();
}

class _MangerScreenState extends State<MangerScreen> {
  DocumentSnapshot? adminSnapshot;
  DocumentSnapshot? using2;
  DocumentSnapshot? subscription;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getAbout();
    getUsing();
    getSubscription();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: Container(
          margin: EdgeInsetsDirectional.only(top: 10),
          child: Text(
            'الادارة',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Color(0xff1DB854),
          ),
          onPressed: () {
            Navigator.pushReplacementNamed(context, 'home_screen');
          },
        ),
      ),
      body: SafeArea(
        child: Align(
          alignment: AlignmentDirectional.topCenter,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 40),
            child: ListView(
              children: [
                SizedBox(
                  height: SizeConfig().scaleHeight(40),
                ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => SettingScreenController(adminSnapshot!),));
                        print('user');
                      },
                      child: selectWidget(
                          widget: Icon(Icons.settings_applications_outlined,size: 60,color: Colors.white,),
                          tittle: ' الاعدادات'),
                    ),
                    SizedBox(
                      height: SizeConfig().scaleHeight(10),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => UserAgreement(using2!),));
                        print('merchant');
                      },
                      child: selectWidget(
                          widget: Icon(Icons.speaker_group_outlined,size: 50,color: Colors.white,),
                          tittle: 'اتفاقية المستخدم'),
                    ),
                SizedBox(
                  height: SizeConfig().scaleHeight(10),
                ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => SubscriptionScreen(subscription!),));
                        print('user');
                      },
                      child: selectWidget(
                          widget: Icon(Icons.monetization_on_outlined,size: 60,color: Colors.white,),
                          tittle: 'الاشتراك'),
                    ),
                SizedBox(
                  height: SizeConfig().scaleHeight(10),
                ),
                    InkWell(
                      onTap: () {
                        Navigator.pushNamed(context, 'read_admins');
                        print('merchant');
                      },
                      child: selectWidget(
                          widget: Icon(Icons.person_outline_rounded,size: 50,color: Colors.white,),
                          tittle: 'المسؤولين'),
                    ),
                ]
            ),
          ),
        ),
      ),
    );
  }

  // Future<void> getAdmin() async{
  //   DocumentSnapshot snapshot = await FirebaseFirestoreController().getAdmin();
  //   setState(() {
  //     adminSnapshot = snapshot;
  //   });
  // }

  Future<void> getAbout() async{
    DocumentSnapshot snapshot = await FirebaseFirestoreController().getAbout();
    setState(() {
      adminSnapshot = snapshot;
    });
  }

  Future<void> getSubscription() async{
    DocumentSnapshot snapshot = await FirebaseFirestoreController().getSubscription();
    setState(() {
      subscription = snapshot;
    });
  }

  Future<void> getUsing() async{
    DocumentSnapshot using = await FirebaseFirestoreController().getUsing();
    setState(() {
      using2 = using;
    });
  }

}
