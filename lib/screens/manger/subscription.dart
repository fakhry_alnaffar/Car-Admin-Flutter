import 'package:car_admin_project/firebase/firebase_firestor_controller.dart';
import 'package:car_admin_project/responsive/size_config.dart';
import 'package:car_admin_project/widgets/app_text_filed_profile.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
class SubscriptionScreen extends StatefulWidget {
  final DocumentSnapshot snapshot;

  SubscriptionScreen(this.snapshot);

  @override
  _SubscriptionScreenState createState() => _SubscriptionScreenState();
}

class _SubscriptionScreenState extends State<SubscriptionScreen> {
  bool isHomeEdit = false;
  bool isMoshrafEdit = false;
  bool isMangerEdit = false;
  late TextEditingController _home;
  late TextEditingController _moshraf;
  late TextEditingController _manger;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _home = TextEditingController(text: widget.snapshot.get('main'));
    _moshraf = TextEditingController(text: widget.snapshot.get('user'));
    _manger = TextEditingController(text: widget.snapshot.get('merchant'));

  }
  @override
  void dispose() {
    // TODO: implement dispose
    _home.dispose();
    _moshraf.dispose();
    _manger.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: Container(
          margin: EdgeInsetsDirectional.only(top: 10),
          child: Text(
            'اسعار الاشتراكات',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Color(0xff1DB854),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          // clipBehavior: Clip.antiAlias,
          physics: BouncingScrollPhysics(),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 0),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 20,),
                  Text('اشتراكات الاعلانات',style: TextStyle(color: Colors.grey.shade600,fontSize: 20,fontWeight: FontWeight.bold),),
                  SizedBox(height: 20,),
                  AppTextFiledProfile(
                    textInputType: TextInputType.number,
                    labelText: 'سعر الاعلان',
                    readOnly: !isHomeEdit,
                    showCursor:isHomeEdit,
                    controller: _home,
                    prefix: Icons.home,
                    suffix: isHomeEdit ? Icons.check : Icons.edit,
                    functionSuffixPressed: () {
                      setState(() {
                        isHomeEdit = !isHomeEdit;
                        if (isHomeEdit == false) {
                          updateMain();
                        }
                      });
                    },
                  ),
                  SizedBox(height: 20,),
                  Text('اشتراكات المستخدمين',style: TextStyle(color: Colors.grey.shade600,fontSize: 20,fontWeight: FontWeight.bold),),
                  SizedBox(height: 20,),
                  AppTextFiledProfile(
                    textInputType: TextInputType.number,
                    labelText: 'اشتراك المشرف',
                    readOnly: !isMoshrafEdit,
                    showCursor:isMoshrafEdit,
                    controller: _moshraf,
                    prefix: Icons.person,
                    suffix: isMoshrafEdit ? Icons.check : Icons.edit,
                    functionSuffixPressed: () {
                      setState(() {
                        isMoshrafEdit = !isMoshrafEdit;
                        if (isMoshrafEdit == false) {
                          updateUser();
                        }
                      });
                    },
                  ),
                  SizedBox(height: 20,),
                  AppTextFiledProfile(
                    textInputType: TextInputType.number,
                    labelText: 'الاشتراك الشهري',
                    readOnly: !isMangerEdit,
                    showCursor:isMangerEdit,
                    controller: _manger,
                    prefix: Icons.person_pin,
                    suffix: isMangerEdit ? Icons.check : Icons.edit,
                    functionSuffixPressed: () {
                      setState(() {
                        isMangerEdit = !isMangerEdit;
                        if (isMangerEdit == false) {
                          updateManager();
                        }
                      });
                    },
                  ),
                ]),
          ),
        ),
      ),
    );
  }

  Future<void> updateMain() async{
    if(_home.text.isNotEmpty)
      await FirebaseFirestoreController().updateAdsMainPrice(path: widget.snapshot.id, main: _home.text);
  }
  Future<void> updateUser() async{
    if(_moshraf.text.isNotEmpty)
      await FirebaseFirestoreController().updateSubUser(path: widget.snapshot.id, user: _moshraf.text);
  }

  Future<void> updateManager() async{
    if(_manger.text.isNotEmpty)
      await FirebaseFirestoreController().updateSubManager(path: widget.snapshot.id, manager: _manger.text);
  }

}
