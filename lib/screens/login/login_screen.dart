import 'package:car_admin_project/firebase/firebase_auth_controller.dart';
import 'package:car_admin_project/firebase/firebase_firestor_controller.dart';
import 'package:car_admin_project/responsive/size_config.dart';
import 'package:car_admin_project/utils/helpers.dart';
import 'package:car_admin_project/widgets/app_text_filed.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with Helpers {
  late TextEditingController _email;
  late TextEditingController _password;
  late bool isPassword = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _email = TextEditingController();
    _password = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _email.dispose();
    _password.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        clipBehavior: Clip.antiAlias,
        padding: EdgeInsetsDirectional.zero,
        physics: BouncingScrollPhysics(),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: 80,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    margin:
                        EdgeInsets.only(bottom: SizeConfig().scaleHeight(0)),
                    child: Image(
                      image: AssetImage("images/logologin.png"),
                    )),
              ],
            ),
            Text(
              'سيارتي',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: Colors.grey,
                letterSpacing: 2,
                wordSpacing: 2,
              ),
            ),
            SizedBox(
              height: 50,
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 35),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    AppTextFiled(
                      labelText: 'البريد الالكتروني\n',
                      controller: _email,
                      textInputType: TextInputType.emailAddress,
                      prefix: Icons.email,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    AppTextFiled(
                      labelText: 'كلمة المرور\n',
                      controller: _password,
                      obscureText: isPassword,
                      prefix: Icons.lock,
                      suffix:
                          isPassword ? Icons.visibility : Icons.visibility_off,
                      functionSuffixPressed: () {
                        setState(() {
                          isPassword = !isPassword;
                        });
                      },
                    ),
                    SizedBox(
                      height: SizeConfig().scaleHeight(30),
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        minimumSize:
                            Size(double.infinity, SizeConfig().scaleHeight(60)),
                        primary: Color(0xff1DB854),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50),
                        ),
                      ),
                      onPressed: () {
                        performSignIn();
                      },
                      child: Text(
                        'تسجيل الدخول',
                        style:
                            TextStyle(fontSize: SizeConfig().scaleTextFont(18)),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> performSignIn() async {
    if (checkData()) {
      await signIn();
    }
  }

  bool checkData() {
    if (_email.text.isNotEmpty && _password.text.isNotEmpty) {
      return true;
    }
    showSnackBar(
        context: context, content: 'ادخل البيانات المطلوبة', error: true);
    return false;
  }

  Future<void> signIn() async {
    bool status = await FirebaseAuthController()
        .signIn(context, email: _email.text, password: _password.text);
    if (status) {
      String type =
          await FirebaseFirestoreController().getUserType(_email.text);
      String state2 =
          await FirebaseFirestoreController().getBlockStateUser2(_email.text);

      if (type == '1') {
        if (state2 == '1') {
          showSnackBar(
              context: context, content: 'للأسف، تم حظر حسابك', error: true);
          print('blocked');
        } else {
          print('opend');
          Navigator.pushReplacementNamed(context, 'home_screen');
        }
      } else {
        showSnackBar(
            context: context,
            content: 'ناسف، لا يمكنك الدخول للوحة التحكم',
            error: true);
      }
    }
  }
}
