import 'package:car_admin_project/firebase/firebase_firestor_controller.dart';
import 'package:car_admin_project/responsive/size_config.dart';
import 'package:car_admin_project/screens/merchant/tab/active_merchant.dart';
import 'package:car_admin_project/screens/merchant/tab/av_merchant.dart';
import 'package:car_admin_project/screens/merchant/tab/block_merchant.dart';
import 'package:car_admin_project/screens/merchant/tab/ex_merchant.dart';
import 'package:car_admin_project/widgets/component.dart';
import 'package:car_admin_project/widgets/loading.dart';
import 'package:car_admin_project/widgets/no_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class MerchantScreen extends StatefulWidget {
  const MerchantScreen({Key? key}) : super(key: key);

  @override
  _MerchantScreenState createState() => _MerchantScreenState();
}

class _MerchantScreenState extends State<MerchantScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = TabController(length: 4, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: Container(
          margin: EdgeInsetsDirectional.only(top: 10),
          child: Text(
            'التجار',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
        bottom: TabBar(
          controller: _tabController,
          labelColor: Color(0xff1DB854),
          labelStyle: TextStyle(fontWeight: FontWeight.bold),
          unselectedLabelStyle: TextStyle(fontWeight: FontWeight.bold),
          indicatorColor: Color(0xff1DB854),
          indicatorWeight: 2,
          indicatorSize: TabBarIndicatorSize.tab,
          tabs: [
            Tab(
              text: 'مجدد',
            ),
            Tab(
              text: 'منتهي',
            ),
            Tab(
              text: 'فعال',
            ),
            Tab(
              text: 'محظور',
            ),
          ],
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Color(0xff1DB854),
          ),
          onPressed: () {
            Navigator.pushReplacementNamed(context, 'home_screen');
          },
        ),
      ),

      //HERE WORK FIREBASE
      body: TabBarView(
        controller: _tabController,
        children: [
          AvMerchant(),
          ExMerchant(),
          ActiveMerchant(),
          BlockMerchant(),
        ],
      ),
    );
  }
}
