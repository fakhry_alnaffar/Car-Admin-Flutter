import 'package:car_admin_project/firebase/firebase_firestor_controller.dart';
import 'package:car_admin_project/widgets/component.dart';
import 'package:car_admin_project/widgets/loading.dart';
import 'package:car_admin_project/widgets/no_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class AvMerchant extends StatefulWidget {
  const AvMerchant({Key? key}) : super(key: key);

  @override
  _AvMerchantState createState() => _AvMerchantState();
}

class _AvMerchantState extends State<AvMerchant> {

  List<String> date = <String>[];
  late TextEditingController _search;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _search = TextEditingController();
    getMerchantDate();
  }

  @override
  void dispose() {
    _search.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 5,
        ),
        SafeArea(
          child: Container(
            margin: EdgeInsetsDirectional.zero,
            padding: EdgeInsetsDirectional.zero,
            // alignment: AlignmentDirectional.topStart,
            child: Padding(
              padding:
              const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
              child: Card(
                // margin: EdgeInsetsDirectional.zero,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50),
                ),
                child: ListTile(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50),
                  ),
                  leading: GestureDetector(
                      onTap: (){
                        //       setState(() {
                        //   _search.text;
                        // });
                      }
                      , child: Icon(Icons.search)),
                  title: TextField(
                    controller: _search,
                    decoration: InputDecoration(
                        hintText: 'البحث', border: InputBorder.none),
                    // onChanged: onSearchTextChanged,
                  ),
                  trailing: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      IconButton(
                        padding: EdgeInsetsDirectional.zero,
                        icon: new Icon(Icons.cancel),
                        onPressed: () {
                          _search.clear();
                          setState(() {
                            _search.text;
                          });
                        },
                      ),
                      IconButton(
                          onPressed: () {
                            // sendMessage();
                            setState(() {
                              _search.text;
                            });
                          },
                          icon: Icon(
                            Icons.send,
                            color: Color(0xff1DB854),
                            textDirection: TextDirection.rtl,
                          )),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: StreamBuilder<QuerySnapshot>(
              stream: _search.text.isNotEmpty ? FirebaseFirestoreController().searchAvailableMerchant(date, _search.text) : FirebaseFirestoreController().readAvailableMerchant(date),
              builder: (context, snapshot) {
                if (snapshot.hasData && snapshot.data!.docs.length > 0) {
                  List<DocumentSnapshot> documents = snapshot.data!.docs;
                  return Container(
                    margin: EdgeInsetsDirectional.zero,
                    padding: EdgeInsetsDirectional.zero,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 24),
                      child: ListView.builder(
                          itemCount: documents.length,
                          physics: BouncingScrollPhysics(),
                          padding: EdgeInsetsDirectional.zero,
                          itemBuilder: (context, index) {
                            return InkWell(
                              child: showItem(
                                widget2: IconButton(
                                    padding: EdgeInsetsDirectional.zero,
                                    onPressed: () {},
                                    icon: Icon(
                                      Icons.update_outlined,
                                      color: Colors.transparent,
                                      size: 25,
                                    )),
                                widget1: IconButton(
                                    padding: EdgeInsetsDirectional.zero,
                                    onPressed: () {launch("tel://${documents[index].get('phoneNumber')}");},
                                    icon: Icon(
                                      Icons.call_rounded,
                                      color: Color(0xff0e06ec),
                                      size: 25,
                                    )),
                                documents: documents,
                                index: index,
                                context: context,
                                title: documents[index].get('marketName'),
                                subtitle: documents[index]
                                    .get('phoneNumber')
                                    .toString(),
                                url: documents[index].get('image'),
                              ),
                              onTap: () {
                                print(index);
                              },
                            );
                            return Container(
                              margin: EdgeInsetsDirectional.zero,
                              padding: EdgeInsetsDirectional.zero,
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 24),
                                child: ListView.builder(
                                    itemCount: documents.length,
                                    physics: BouncingScrollPhysics(),
                                    padding: EdgeInsetsDirectional.zero,
                                    itemBuilder: (context, index) {
                                      return InkWell(
                                        child: showItem(
                                          widget1: IconButton(
                                              padding:
                                                  EdgeInsetsDirectional.zero,
                                              onPressed: () {},
                                              icon: Icon(
                                                Icons.update_outlined,
                                                color: Color(0xffff5100),
                                                size: 25,
                                              )),
                                          widget2: IconButton(
                                              padding:
                                                  EdgeInsetsDirectional.zero,
                                              onPressed: () {},
                                              icon: Icon(
                                                Icons.call_rounded,
                                                color: Color(0xff0e06ec),
                                                size: 25,
                                              )),
                                          documents: documents,
                                          index: index,
                                          context: context,
                                          title: documents[index]
                                              .get('marketName'),
                                          subtitle: documents[index]
                                              .get('phoneNumber')
                                              .toString(),
                                          url: documents[index].get('image'),
                                        ),
                                        onTap: () {
                                          print(index);
                                        },
                                      );
                                    }),
                              ),
                            );
                          }),
                    ),
                  );
                } else if (snapshot.connectionState ==
                    ConnectionState.waiting) {
                  return Loading();
                } else {
                  return NoData();
                }
              }),
        ),
      ],
    );
  }

  Future<void> getMerchantDate() async{
    List<String> list = await FirebaseFirestoreController().getMerchantDate();
    setState(() {
      date = list;
    });
  }

}
