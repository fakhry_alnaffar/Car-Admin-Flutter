import 'package:car_admin_project/firebase/firebase_firestor_controller.dart';
import 'package:car_admin_project/responsive/size_config.dart';
import 'package:car_admin_project/widgets/component.dart';
import 'package:car_admin_project/widgets/loading.dart';
import 'package:car_admin_project/widgets/no_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
class BlockMerchant extends StatefulWidget {
  const BlockMerchant({Key? key}) : super(key: key);

  @override
  _BlockMerchantState createState() => _BlockMerchantState();
}

class _BlockMerchantState extends State<BlockMerchant> {

  List<String> date = <String>[];
  late TextEditingController _search;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _search = TextEditingController();
    getMerchantDate();
  }

  @override
  void dispose() {
    _search.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 5,
        ),
        SafeArea(
          child: Container(
            margin: EdgeInsetsDirectional.zero,
            padding: EdgeInsetsDirectional.zero,
            // alignment: AlignmentDirectional.topStart,
            child: Padding(
              padding:
              const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
              child: Card(
                // margin: EdgeInsetsDirectional.zero,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50),
                ),
                child: ListTile(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50),
                  ),
                  leading: GestureDetector(
                      onTap: (){
                        //       setState(() {
                        //   _search.text;
                        // });
                      }
                      , child: Icon(Icons.search)),
                  title: TextField(
                    controller: _search,
                    decoration: InputDecoration(
                        hintText: 'البحث', border: InputBorder.none),
                    // onChanged: onSearchTextChanged,
                  ),
                  trailing: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      IconButton(
                        padding: EdgeInsetsDirectional.zero,
                        icon: new Icon(Icons.cancel),
                        onPressed: () {
                          _search.clear();
                          setState(() {
                            _search.text;
                          });
                        },
                      ),
                      IconButton(
                          onPressed: () {
                            // sendMessage();
                            setState(() {
                              _search.text;
                            });
                          },
                          icon: Icon(
                            Icons.send,
                            color: Color(0xff1DB854),
                            textDirection: TextDirection.rtl,
                          )),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: StreamBuilder<QuerySnapshot>(
              stream: _search.text.isNotEmpty ? FirebaseFirestoreController().searchBlockMerchant(date, _search.text) : FirebaseFirestoreController().readBlockMerchant2(),
              builder: (context, snapshot) {
                if (snapshot.hasData && snapshot.data!.docs.length > 0) {
                  List<DocumentSnapshot> documents = snapshot.data!.docs;
                  return Container(
                    margin: EdgeInsetsDirectional.zero,
                    padding: EdgeInsetsDirectional.zero,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 24),
                      child: ListView.builder(
                          itemCount: documents.length,
                          physics: BouncingScrollPhysics(),
                          padding: EdgeInsetsDirectional.zero,
                          itemBuilder: (context, index) {
                            return InkWell(
                              child: showItem(
                                widget1: IconButton(
                                    padding: EdgeInsetsDirectional.zero,
                                    onPressed: ()async {

                                      showDialog(
                                          barrierColor: Colors.black.withOpacity(0.16),
                                          context: context,
                                          builder: (context) {
                                            return AlertDialog(
                                              backgroundColor: Color(0xff1DB854),
                                              clipBehavior: Clip.antiAlias,
                                              contentPadding:
                                              EdgeInsetsDirectional.zero,
                                              actionsPadding:
                                              EdgeInsetsDirectional.zero,
                                              title: Center(
                                                  child: Text(
                                                    'هل انت متاكد ؟',
                                                    style: TextStyle(color: Colors.white),
                                                  )),
                                              actions: [
                                                Container(
                                                  alignment:
                                                  AlignmentDirectional.bottomStart,
                                                  margin: EdgeInsetsDirectional.only(
                                                      start: 0,
                                                      end: 0,
                                                      bottom: 0,
                                                      top: 0),
                                                  padding: EdgeInsetsDirectional.zero,
                                                  height: 50,
                                                  color: Colors.white70,
                                                  child: Stack(children: [
                                                    Column(
                                                      children: [
                                                        SizedBox(
                                                          height: 50,
                                                          child: Align(
                                                            alignment:
                                                            Alignment.bottomCenter,
                                                            child: Container(
                                                              alignment:
                                                              AlignmentDirectional
                                                                  .centerEnd,
                                                              height: 50,
                                                              padding:
                                                              EdgeInsetsDirectional
                                                                  .zero,
                                                              width: double.infinity,
                                                              color: Colors.transparent,
                                                              child: Row(children: [
                                                                Expanded(
                                                                  child: TextButton(
                                                                    onPressed:
                                                                        () async {
                                                                      Navigator.pop(
                                                                          context);
                                                                      await FirebaseFirestoreController().updateToActiveMerchant(path: documents[index].id);
                                                                    },
                                                                    child: Text(
                                                                      'نعم',
                                                                      style: TextStyle(
                                                                          color: Colors
                                                                              .white,
                                                                          fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                          fontSize: 18),
                                                                    ),
                                                                    style:
                                                                    ElevatedButton
                                                                        .styleFrom(
                                                                      minimumSize: Size(
                                                                          double
                                                                              .infinity,
                                                                          SizeConfig()
                                                                              .scaleHeight(
                                                                              60)),
                                                                      primary: Color(
                                                                          0xff1DB854),
                                                                      shape:
                                                                      RoundedRectangleBorder(
                                                                        borderRadius:
                                                                        BorderRadius
                                                                            .circular(
                                                                            0),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                                SizedBox(
                                                                  width: 1,
                                                                ),
                                                                Expanded(
                                                                  child: TextButton(
                                                                    onPressed: () {
                                                                      Navigator.pop(
                                                                          context);
                                                                    },
                                                                    child: Text(
                                                                      'لا',
                                                                      style: TextStyle(
                                                                          color: Colors
                                                                              .white,
                                                                          fontSize: 18,
                                                                          fontWeight:
                                                                          FontWeight
                                                                              .bold),
                                                                    ),
                                                                    style:
                                                                    ElevatedButton
                                                                        .styleFrom(
                                                                      minimumSize: Size(
                                                                          double
                                                                              .infinity,
                                                                          SizeConfig()
                                                                              .scaleHeight(
                                                                              60)),
                                                                      primary: Color(
                                                                          0xff1DB854),
                                                                      shape:
                                                                      RoundedRectangleBorder(
                                                                        borderRadius:
                                                                        BorderRadius
                                                                            .circular(
                                                                            0),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ]),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ]),
                                                ),
                                              ],
// actionsPadding: EdgeInsets.symmetric(horizontal: 50),
                                              shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(15),
                                              ),
                                            );
                                          });

                                      },
                                    icon: Icon(
                                      Icons.cancel_outlined,
                                      color: Color(0xff1DB854),
                                      size: 25,
                                    )),
                                widget2: IconButton(
                                    padding: EdgeInsetsDirectional.zero,
                                    onPressed: () {launch("tel://${documents[index].get('phoneNumber')}");},
                                    icon: Icon(
                                      Icons.call_rounded,
                                      color: Color(0xff0e06ec),
                                      size: 25,
                                    )),
                                documents: documents,
                                index: index,
                                context: context,
                                title: documents[index].get('marketName'),
                                subtitle: documents[index]
                                    .get('place')
                                    .toString(),
                                url: documents[index].get('image'),
                              ),
                              onTap: () {
                                print(index);
                              },
                            );
                          }),
                    ),
                  );
                } else if (snapshot.connectionState ==
                    ConnectionState.waiting) {
                  return Loading();
                } else {
                  return NoData();
                }
              }),
        ),
      ],
    );
  }

  Future<void> getMerchantDate() async{
    List<String> list = await FirebaseFirestoreController().getMerchantDate();
    setState(() {
      date = list;
    });
  }

}
