import 'package:car_admin_project/firebase/firebase_firestor_controller.dart';
import 'package:car_admin_project/models/dashboard_items.dart';
import 'package:car_admin_project/responsive/size_config.dart';
import 'package:car_admin_project/screens/setting/profile_screen.dart';
import 'package:car_admin_project/screens/setting/setting_screen_controller.dart';
import 'package:car_admin_project/utils/helpers.dart';
import 'package:car_admin_project/widgets/component.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with Helpers {
  List<DashboardItem> items = <DashboardItem>[];
  DocumentSnapshot? adminSnapshot;
  DocumentSnapshot? using2;
  String name = '';
  String url = '';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getAdmin();
    getUsing();
    getUserName();
    getUserImage();
    check();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    items = <DashboardItem>[
      DashboardItem(
          title: 'طلبات',
          icon: Icons.article_rounded,
          iconColor: Color(0xffF26950)),
      DashboardItem(
          title: 'الادارة',
          icon: Icons.meeting_room_rounded,
          iconColor: Color(0xff30e208)),
      DashboardItem(
          title: 'عملاء',
          icon: Icons.person,
          iconColor: Color(0xff6e59e5)),
      DashboardItem(
          title: 'التجار',
          icon: Icons.import_contacts_outlined,
          iconColor: Colors.redAccent),
    ];
    return Scaffold(
      endDrawerEnableOpenDragGesture: true,
      drawerEdgeDragWidth: 10,
      drawerEnableOpenDragGesture: true,
      extendBody: true,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
              tooltip: MaterialLocalizations
                  .of(context)
                  .openAppDrawerTooltip,
              icon: Container(
                margin: EdgeInsets.symmetric(vertical: 5),
                child: Image(
                  image: AssetImage(
                    "images/aa.png",
                  ),
                ),
              ),
            );
          },
        ),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(20),
              bottom: SizeConfig().scaleWidth(0),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            'لوحة التحكم',
            style: TextStyle(
                letterSpacing: SizeConfig().scaleWidth(2),
                wordSpacing: SizeConfig().scaleWidth(0.5),
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
      ),
      drawer: ClipRRect(
        borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(20.0), topRight: Radius.circular(20)),
        child: Container(
          color: Color(0xff1DB854),
          width: 310,
          child: Drawer(
            child: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: AlignmentDirectional.topStart,
                  end: AlignmentDirectional.bottomEnd,
                  colors: [
                    Color(0xff1DB854),
                    Color(0xff1DB854),
                    Color(0x8b1db854),
                  ],
                ),
              ),
              child: ListView(
                clipBehavior: Clip.antiAliasWithSaveLayer,
                physics: NeverScrollableScrollPhysics(),
                padding: EdgeInsets.symmetric(vertical: 10),
                primary: true,
                children: [
                  UserAccountsDrawerHeader(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(0),
                      gradient: LinearGradient(
                        begin: AlignmentDirectional.topStart,
                        end: AlignmentDirectional.bottomEnd,
                        colors: [
                          Color(0xff1DB854),
                          Color(0xff1DB854),
                          Color(0x8b1db854),
                        ],
                      ),
                    ),
                    accountName: Text(name),
                    accountEmail: Text(
                        FirebaseAuth.instance.currentUser!.email.toString()),
                    currentAccountPicture: CircleAvatar(
                      backgroundColor: Colors.white,
                      backgroundImage: NetworkImage(url),
                    ),
                    arrowColor: Colors.black,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  ListTile(
                    // isThreeLine: true,
                    dense: true,
                    selected: false,
                    leading: Icon(
                      Icons.settings_applications,
                      size: 30,
                      color: Colors.white,
                    ),
                    title: Container(
                        margin: EdgeInsetsDirectional.only(bottom: 0),
                        padding: EdgeInsetsDirectional.only(bottom: 0),
                        child: Text(
                          'الاعدادت',
                          style: TextStyle(fontSize: 18, color: Colors.white),
                        )),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.push(context, MaterialPageRoute(builder: (
                          context) => ProfileScreen(),));
                    },
                  ),
                  SizedBox(height: SizeConfig().scaleHeight(10)),
                  ListTile(
                    dense: true,
                    selected: false,
                    leading: Icon(
                      Icons.login_outlined,
                      size: 30,
                      color: Colors.white,
                    ),
                    title: Container(
                        margin: EdgeInsetsDirectional.only(bottom: 0),
                        child: Text(
                          'تسجيل خروج',
                          style: TextStyle(fontSize: 18, color: Colors.white),
                        )),
                    onTap: () {
                      signOut();
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xff1DB854),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
            child: Container(
              margin: EdgeInsets.only(top: SizeConfig().scaleHeight(120)),
              width: double.infinity,
              height: double.infinity,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xffF0F4FD),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(SizeConfig().scaleWidth(40)),
                  topRight: Radius.circular(SizeConfig().scaleWidth(40)),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: GridView.builder(
                    itemCount: items.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        mainAxisSpacing: SizeConfig().scaleHeight(24),
                        crossAxisSpacing: SizeConfig().scaleHeight(24),
                        childAspectRatio: SizeConfig().scaleHeight(171) /
                            SizeConfig().scaleWidth(171),
                        mainAxisExtent: SizeConfig().scaleHeight(171)),
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          if (index == 0) {
                            print('order');
                            Navigator.pushNamed(
                                context, 'order_screen');
                          } else if (index == 1) {
                            print('manger');
                            Navigator.pushNamed(
                                context, 'manger_screen');
                          } else if (index == 2) {
                            Navigator.pushNamed(
                                context, 'customer_screen');
                            print('customer');
                          } else if (index == 3) {
                            Navigator.pushNamed(
                                context, 'merchant_screen');
                            print('merchant');
                          } else if (index == 4) {
                          }
                        },
                        child: homeWidget(
                          icon: items[index].icon,
                          tittle: items[index].title,
                          iconColor: items[index].iconColor,
                        ),
                      );
                    }),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> signOut() async {
    await FirebaseAuth.instance.signOut();
    Navigator.pushReplacementNamed(context, 'login_screen');
  }
  Future<void> getAdmin() async {
    DocumentSnapshot snapshot = await FirebaseFirestoreController().getAdmin();
    setState(() {
      adminSnapshot = snapshot;
    });
  }
  Future<void> getUsing() async {
    DocumentSnapshot using = await FirebaseFirestoreController().getUsing();
    setState(() {
      using2 = using;
    });
  }
  Future<void> getUserName() async {
    String namee = await FirebaseFirestoreController().getName();
    setState(() {
      name = namee;
    });
  }
  Future<void> getUserImage() async {
    String urlImage = await FirebaseFirestoreController().getUserImage();
    setState(() {
      url = urlImage;
    });
  }
  Future<void> check() async {
    String state2 =
    await FirebaseFirestoreController().getBlockStateUser2(FirebaseAuth.instance.currentUser!.email.toString());
    if (state2 == '1') {
      showSnackBar(
          context: context, content: 'للأسف، تم حظر حسابك', error: true);
      print('go out');
      signOut();
    } else {
      print('opend');
    }
  }
}

